## eslint

```shell
npx eslint --init

pnpm i eslint-plugin-vue@latest @typescript-eslint/eslint-plugin@latest @typescript-eslint/parser@latest eslint@latest -D -w

# 支持 vue 中 用 eslint 修复

pnpm i @vue/eslint-config-typescript -D -w
```
