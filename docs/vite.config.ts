import { defineConfig } from 'vite'
import DefineOptions from 'unplugin-vue-define-options/dist/vite.js'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [DefineOptions()],
})
