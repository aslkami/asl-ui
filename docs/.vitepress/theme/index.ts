import DefaultTheme from 'vitepress/theme'
import '@asl-ui/theme-chalk/index.scss'
import AslIcon from '@asl-ui/components/icon'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(AslIcon); // 注册组件
  }
}