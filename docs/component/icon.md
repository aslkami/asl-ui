# Icon 图标

asl-ui 推荐使用 xicons 作为图标库。

```
$ pnpm install @vicons/antd
```

## 使用图标

- 如果你想像用例一样直接使用，你需要全局注册组件，才能够直接在项目里使用。

<script setup lang="ts">
import { DingdingOutlined } from '@vicons/antd'
</script>
<AslIcon color="red" size="40">
  <DingdingOutlined/>
</AslIcon>

<AslIcon color="green" size="40">
  <DingdingOutlined/>
</AslIcon>
<AslIcon color="blue" size="40">
  <DingdingOutlined/>
</AslIcon>
<div>

<AslIcon color="red" size="60">
  <DingdingOutlined/>
</AslIcon>

<AslIcon color="green" size="60">
  <DingdingOutlined/>
</AslIcon>

<AslIcon color="blue" size="60">
  <DingdingOutlined/>
</AslIcon>
</div>

```vue
<script setup lang="ts">
import { DingdingOutlined } from '@vicons/antd';
</script>
<template>
  <AslIcon color="red" size="40">
    <DingdingOutlined />
  </AslIcon>
</template>
```

## API

### Icon Props

| 名称  | 类型             | 默认值    | 说明     |
| ----- | ---------------- | --------- | -------- |
| color | string           | undefined | 图标颜色 |
| size  | number \| string | undefined | 图片大小 |
