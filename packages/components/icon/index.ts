import { withInstall } from '@asl-ui/utils/with-install'
import _ICon from './src/icon.vue'

const Icon = withInstall(_ICon)
export default Icon
export * from './src/icon'


declare module 'vue' {
  export interface GlobalComponents {
    AslIcon: typeof Icon
  }
}