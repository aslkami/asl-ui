import type { ExtractPropTypes, PropType } from "vue"

export const iconProps = {
  color: String,
  size: [Number, String] as PropType<number | string>
} as const

type IP = typeof iconProps;
// 将构造函数转化成 普通类型
export type IconProps = ExtractPropTypes<IP>