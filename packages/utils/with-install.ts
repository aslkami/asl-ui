import { Plugin, Component } from 'vue'

export type SFCwithInstall<T> = T & Plugin
export function withInstall<T>(comp: T) {
  (comp as SFCwithInstall<T>).install = (app) => {
    const { name } = comp as { name: string }
    app.component(name, comp as Component) //! 这里被迫指定是 Component， 有机会再研究
  }

  return comp as SFCwithInstall<T>
}
