import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import '@asl-ui/theme-chalk/index.scss'
import Icon from '@asl-ui/components/icon'

const app = createApp(App)

const plugins = [
  Icon
]

plugins.forEach((plugin) => app.use(plugin))

app.mount('#app')
